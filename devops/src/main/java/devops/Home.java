package devops;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Home extends JFrame{

	boolean showPage = true;
	int width = 800;
	int heigth = 600;
	
	public Home() {
		// TODO Auto-generated constructor stub
		this.setVisible(showPage);
		this.setSize(width, heigth);
		this.setTitle("Accueil");
		WebService webservice = new WebService();
		JsonObject data = new JsonObject();
		JsonArray Nom = new JsonArray();
		JsonArray NomVoie = new JsonArray();
		JsonArray NumVoie = new JsonArray();
		
		JPanel panel = new JPanel();
		JButton btn_Refresh = new JButton("Rechercher");
		
		try {
			data = webservice.GetData();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		JsonArray values = (JsonArray) data.get("values");
		//System.out.println(values);
		try {
			for (JsonElement jsonElement : values) {
				Nom.add(jsonElement.getAsJsonObject().get("nom"));
				NomVoie.add(jsonElement.getAsJsonObject().get("voie"));
				NumVoie.add(jsonElement.getAsJsonObject().get("numvoie"));
				
			}
		} catch (NullPointerException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		
		JTable tbl = new JTable();

	    DefaultTableModel dm = new DefaultTableModel(0, 0);
	    String header[] = new String[] {"ID", "Nom", "Numero", "Voie"};
	    dm.setColumnIdentifiers(header);
	    tbl.setModel(dm);
	    int index;
	    for (index = 0; index<20;index++) {
	        Vector<Object> vector = new Vector<Object>();
	        vector.add(index +1);
	        vector.add(Nom.get(index).getAsString());
	        vector.add(NumVoie.get(index).getAsString());
	        vector.add(NomVoie.get(index).getAsString());
	        dm.addRow(vector);
		}

		btn_Refresh.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				System.out.println("Actualisation");
			}
		});
		
		panel.add(btn_Refresh);
		panel.add(tbl);
		
		this.add(panel);
	}
	
}
