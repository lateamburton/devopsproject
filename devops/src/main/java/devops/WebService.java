package devops;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class WebService {

	public JsonObject GetData() throws IOException {
		URL url = null;
		try {
			url = new URL("https://download.data.grandlyon.com/ws/grandlyon/com_donnees_communales.comparcjardin_1_0_0/all.json?maxfeatures=100&start=1");
			//System.out.println(url);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		try {
			con.setRequestMethod("GET");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		String inputLine;
		StringBuffer content = new StringBuffer();
		BufferedReader in = new BufferedReader(
				  new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
				    content.append(inputLine);
				}
				in.close();
		con.disconnect();
		System.out.println(content);
		JsonObject jsonObject = new JsonParser().parse(content.toString()).getAsJsonObject();
		return jsonObject;
	}
	
}
